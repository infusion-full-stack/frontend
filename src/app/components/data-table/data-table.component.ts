import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { User, Users } from 'src/app/users/models/user.model';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnInit {

  @Input() displayedColumns = [];
  @Input() users: any;
  @Output() outSelectRow = new EventEmitter<User>();
  @Output() outDeleteUser = new EventEmitter<String>();
  @Output() outUpdateUser = new EventEmitter<any>();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource: MatTableDataSource<any>;
  selectedRowValue: any;
  idRowOver: string;

  constructor() { }
  ngOnInit(): void {
  }

  ngOnChanges(): void {
    this.dataSource = new MatTableDataSource<any>(this.users);
    this.selectedRowValue = this.users && this.users[0] || ''
    this.dataSource.paginator = this.paginator;
    this.onSelectedRow(this.selectedRowValue)
  }

  onSelectedRow(user: User) {
    this.selectedRowValue = user;
    this.outSelectRow.emit(this.selectedRowValue)
  }

  onOverRow(user: User) {
    this.idRowOver = user._id
  }

  onMouseOut() {
    this.idRowOver = ''
  }

  onDeleteUser(id: String) {
    this.outDeleteUser.emit(id);
  }

  onUpdateUser(user) {
    this.outUpdateUser.emit(user);
  }
}


