import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Users } from '../models/user.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) { }

  getUser(params) {

    const HttpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      params 
    };
    return this.httpClient.get<Users>(environment.API_URL + 'users',HttpOptions)
  }

  saveUser(body: Users) {
    return this.httpClient.post<Users>(environment.API_URL + 'users', body);
  }

  updateUser(params) {
    return this.httpClient.put(`${environment.API_URL}users/${params._id}`, params);
  }

  deleteUser(id) {
    return this.httpClient.delete(`${environment.API_URL}users/${id}`);
  }

}
