export interface Users {
  users: User;
}

export interface User {
  role: string;
  status: string;
  creationDate: string;
  _id: string;
  name: string;
  email: string;
}