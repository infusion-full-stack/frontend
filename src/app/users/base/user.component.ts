import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogConfirmationComponent } from 'src/app/components/dialog-confirmation/dialog-confirmation.component';
import { Users } from '../models/user.model';
import { UserService } from '../services/user.service';



export interface Section {
  name: string;
  updated: Date;
}@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  public users: Users;
  public displayedColumns: string[] = [ 'name', 'email', 'status', 'role','creationDate', 'options'];
  public form: FormGroup


  public itemsRole = [
    { id: 1, role: 'Admin' },
    { id: 2, role: 'User' },
  ]

  public itemsStatus = [
    { id: 1, status: 'Activo' },
    { id: 2, status: 'Inactivo' },
  ]
  loading: boolean;
  currentAction = { id: 1, info: 'Detalle de Usuario' }
  seletedUserData: Users;
  formFilter: FormGroup;
  filter;

  constructor(private _usersServices: UserService,
    private fb: FormBuilder,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.createForm(false)
    this.getUser()
  }

  async openDialogConfirmation() {
    const dialogRef = this.dialog.open(DialogConfirmationComponent, {
      width: '350px',
    });
    const result = await dialogRef.afterClosed().toPromise()
    return result
  }

  createForm(enable) {
    this.form = this.fb.group({
      _id: [{ value: '' }],
      name: [{ value: '', disabled: true }, Validators.required],
      email: [{ value: '', disabled: true }, [Validators.required, Validators.email]],
      password: [{ value: '', disabled: true }, [Validators.required, Validators.minLength(6)]],
      role: [{ value: this.itemsRole[0].role, disabled: true }, Validators.required],
      status: [{ value: this.itemsStatus[0].status, disabled: true }, Validators.required],
    })

    this.formFilter = this.fb.group({
      status: [this.itemsStatus[0].status],
    })

    if (enable) {
      this.form.enable()
    }
  }

  selectedUser(user: Users) {
    this.seletedUserData = user
    this.setDataForm(user);
  }

  setDataForm(user) {

    if(!user){
      this.form.reset()
    }
    setTimeout(() => {
      this.form.patchValue(user)
    }, 0)
  }

  getUser() {
    this.loading = true;
    this._usersServices.getUser(this.formFilter.value)
      .subscribe((res: any) => {
        this.users = res.users;
        this.loading = false;
        this.currentAction={id:1, info:'Detalle de Usuario'}
      });
  }

  changeFilter(){
    this.getUser()
  }

  saveUser() {
    this.loading = true;
    if (this.currentAction.id == 2) {
      this.sendCreateRequest()
      return
    }

    if (this.currentAction.id == 3) {
      this.sendUpdateRequest()
    }

  }


  sendUpdateRequest() {
    this._usersServices.updateUser(this.form.value).subscribe((resp: any) => {
      this.openSnackBar(resp.msg)
      this.getUser()
      this.form.disable()
      this.loading = false;
    }, error => {
      console.log(error)
      let parseError = error.error.error ? error.error.error : error.error.errors[0].param + ' ' + error.error.errors[0].msg;
      this.openSnackBar(parseError)
      this.loading = false;
    })
  }

  sendCreateRequest() {
    this._usersServices.saveUser(this.form.value).subscribe((resp: any) => {
      this.openSnackBar(resp.msg)
      this.getUser()
      this.form.disable()
      this.loading = false;
    }, error => {
      console.log(error)
      let parseError = error.error.error ? error.error.error : error.error.errors[0].param + ' ' + error.error.errors[0].msg;
      this.openSnackBar(parseError)
      this.loading = false;
    })
  }


  createUser() {
    this.currentAction = { id: 2, info: 'Registro de Usuario' }
    this.form.enable()
    this.createForm(true)
  }

  deleteUser(id) {
    this.openDialogConfirmation().then(result => {
      if (result) {
        this.loading = true;
        this._usersServices.deleteUser(id)
          .subscribe((res: any) => {
            this.loading = false;
            this.getUser()
            this.openSnackBar(res.msg)
          })
      } error => {
        console.log(error);
      }
    })

  }

  updateUser(user) {
    this.currentAction = { id: 3, info: 'Editar Usuario' }
    this.setDataForm(user)
    this.form.enable()
  }

  cancelar() {
    this.setDataForm(this.seletedUserData)
    this.currentAction = { id: 1, info: 'Detalle de Usuario' }
    this.form.disable()
  }

  openSnackBar(msg: string) {
    this._snackBar.open(msg, 'Cerrar', {
      duration: 5 * 1000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
    });
  }
}


